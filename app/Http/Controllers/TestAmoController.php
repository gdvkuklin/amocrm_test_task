<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;
use App\Models\AccessToken as modelAccessToken;
use App\Models\Lead;

use AmoCRM\Models\LeadModel;
use AmoCRM\Collections\CustomFieldsValuesCollection;
use AmoCRM\Models\CustomFieldsValues\TextCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\NumericCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\TextCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\NumericCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueModels\TextCustomFieldValueModel;
use AmoCRM\Models\CustomFieldsValues\ValueModels\NumericCustomFieldValueModel;

class TestAmoController extends Controller
{
    private $apiClient;

    public function __construct(){
        $token = modelAccessToken::where('name','main_access_token')->first();

        $accessToken = new AccessToken([
            'access_token' => $token->access_token,
            'refresh_token' => $token->refresh_token,
            'expires' => $token->expires,
            'baseDomain' => $token->base_domain,
        ]);

        $this->apiClient = new \AmoCRM\Client\AmoCRMApiClient(env('CLIENT_ID'), env('CLIENT_SECRET'), env('CLIENT_REDIRECT_URI'));
        $this->apiClient->setAccessToken($accessToken)
        ->setAccountBaseDomain($accessToken->getValues()['baseDomain'])
        ->onAccessTokenRefresh(
            function (AccessTokenInterface $accessToken, string $baseDomain) {
                $token = 'App\Models\AccessToken'::where('name','main_access_token')->first();

                $token->access_token = $accessToken->getToken();
                $token->refresh_token = $accessToken->getRefreshToken();
                $token->expires = $accessToken->getExpires();
                $token->base_domain = $baseDomain;

                $token->save();
            }
        );
    }

    public function handleAmoEvents(Request $request) {
        try {
            if (isset($request->leads['update']) and count($request->leads['update'])>0){
                foreach($request->leads['update'] as $lead) {

                    $oldLead = Lead::where('amo_id',$lead['id'])->first();
                    $oldLeadIsCreated = false;
                    $cost = 0;

                    if (collect($lead['custom_fields'])->firstWhere('name','Себестоимость')['values'][0]['value']) {
                        $cost = collect($lead['custom_fields'])->firstWhere('name','Себестоимость')['values'][0]['value'];
                    }

                    $price = 0;
                    if (isset($lead['price'])) {
                        $price = $lead['price'];
                    }
                    
                    if ($oldLead == null) {
                        $oldLead = Lead::create([
                            'amo_id'=>$lead['id'],
                            'price'=>$price,
                            'cost'=>$cost,
                        ]);
                        
                        $oldLeadIsCreated = true;
                    }
    
                    if ($lead['price'] == $oldLead->price and $cost == $oldLead->cost and !$oldLeadIsCreated) {
                        return;
                    }
                    
                    if (!$oldLeadIsCreated) {
                        $oldLead->price = $lead['price'];
                        $oldLead->cost = $cost;
                        $oldLead->save();
                    }
    
                    $profit = $price - $cost;
                    if ($profit <= 0) {
                        $profit = 0;
                        $cost = $price;
                    }
    
                    $amoLead = $this->apiClient->leads()->getOne($lead['id']);
                    $customFields = $amoLead->getCustomFieldsValues();
                    $costField = $customFields->getBy('fieldId', 1398697);
                    $profitField = $customFields->getBy('fieldId', 1398699);

                    if (!$costField) {
                        $costField = new NumericCustomFieldValuesModel();
                        $costField->setFieldId(1398697);
                        $costField->setFieldName('Себестоимость');
                        $customFields->add($costField);
                    }
    
                    if (!$profitField) {
                        $profitField = new NumericCustomFieldValuesModel();
                        $profitField->setFieldId(1398699);
                        $profitField->setFieldName('Прибыль');
                        $customFields->add($profitField);
                    }
                    
                    $costField->setValues(
                        (new NumericCustomFieldValueCollection())
                            ->add(
                                (new NumericCustomFieldValueModel())
                                    ->setValue($cost)
                            )
                    );
                    
                    $profitField->setValues(
                        (new NumericCustomFieldValueCollection())
                            ->add(
                                (new NumericCustomFieldValueModel())
                                    ->setValue($profit)
                            )
                    );
    
                    $this->apiClient->leads()->updateOne($amoLead);
                }
            }
        } catch(\Exception $e) {
            info("Ошибка amo_api: ".$e);
        }
    }
}
